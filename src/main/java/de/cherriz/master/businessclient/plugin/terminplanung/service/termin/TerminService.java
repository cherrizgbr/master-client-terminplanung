package de.cherriz.master.businessclient.plugin.terminplanung.service.termin;

import java.util.List;

/**
 * Service zum Zugriff auf den REST Service für Termine.
 *
 * @author Frederik Kirsch
 */
public interface TerminService {

    /**
     * Liefert Termine eines Vertreters.
     *
     * @param id Die ID des Vertreters.
     *
     * @return Die Termine des Vertreters.
     */
    List<Termin> getTermineByVertreter(Long id);

    /**
     * Speichert einen Termin und liefert diesen ergaenzt um ID zurueck.
     *
     * @param termin Der zu speichernde Termin.
     *
     * @return Der gespeicherte Termin
     */
    Termin create(Termin termin);

}