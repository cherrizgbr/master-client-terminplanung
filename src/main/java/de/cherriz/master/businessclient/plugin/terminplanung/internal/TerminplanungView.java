package de.cherriz.master.businessclient.plugin.terminplanung.internal;

import de.cherriz.master.businessclient.dialog.PluginView;
import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.plugin.terminplanung.service.termin.Termin;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

/**
 * ViewController für die UI des Plugins Terminplanung.
 *
 * @author Frederik Kirsch
 */
public class TerminplanungView implements PluginView, EventHandler<ActionEvent> {

    @FXML
    private BorderPane root;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtAdresse;

    @FXML
    private TextField txtTelefon;

    @FXML
    private TextField txtEmail;

    @FXML
    private ListView<Termin> lstCalendar;

    @FXML
    private DatePicker dpDate;

    @FXML
    private ComboBox<Zeitblock> cbxTime;

    @FXML
    private Button btnAbbrechen;

    @FXML
    private Button btnFake;

    @FXML
    private Button btnTermin;

    @FXML
    private Button btnKeinTermin;

    @FXML
    private Button btnInfo;

    private TerminplanungViewModel viewModel = null;

    private final ObjectProperty<EventHandler<ActionEvent>> cancelProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> fakeProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> terminProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> keinTerminProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> infoProperty = new SimpleObjectProperty<>();

    @Override
    public void setViewModel(ViewModel model) {
        this.viewModel = model.getCastetModel();

        //TextFelder
        this.txtName.textProperty().bind(Bindings.concat(this.viewModel.vornameProperty()).concat(" ").concat(this.viewModel.nachnameProperty()));
        this.txtAdresse.textProperty().bind(Bindings.concat(this.viewModel.strasseProperty()).concat(", ").concat(this.viewModel.plzProperty()).concat(" ").concat(this.viewModel.ortProperty()));
        this.txtEmail.textProperty().bind(this.viewModel.mailProperty());
        this.txtTelefon.textProperty().bind(this.viewModel.telefonProperty());
        this.cbxTime.itemsProperty().bind(this.viewModel.zeitenProperty());
        this.lstCalendar.itemsProperty().bind(this.viewModel.termineProperty());

        this.viewModel.dateProperty().bind(this.dpDate.valueProperty());
        this.viewModel.timeProperty().bind(this.cbxTime.getSelectionModel().selectedItemProperty());

        //Buttons
        this.cancelProperty.set(this);
        this.btnAbbrechen.onActionProperty().bind(this.cancelProperty);
        this.fakeProperty.set(this);
        this.btnFake.onActionProperty().bind(this.fakeProperty);
        this.terminProperty.set(this);
        this.btnTermin.onActionProperty().bind(this.terminProperty);
        this.keinTerminProperty.set(this);
        this.btnKeinTermin.onActionProperty().bind(this.keinTerminProperty);
        this.infoProperty.set(this);
        this.btnInfo.onActionProperty().bind(this.infoProperty);
        this.btnTermin.disableProperty().bind(Bindings.or(this.dpDate.valueProperty().isNull(), this.cbxTime.getSelectionModel().selectedItemProperty().isNull()));
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource().equals(this.btnAbbrechen)) {
            this.viewModel.cancel();
        } else if (event.getSource().equals(this.btnFake)) {
            this.viewModel.fake();
        } else if (event.getSource().equals(this.btnTermin)) {
            this.viewModel.termin();
        } else if (event.getSource().equals(this.btnKeinTermin)) {
            this.viewModel.keinTermin();
        } else if (event.getSource().equals(this.btnInfo)) {
            this.viewModel.info();
        }
    }

    @Override
    public Pane getRoot() {
        return this.root;
    }

}