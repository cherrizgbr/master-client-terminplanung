package de.cherriz.master.businessclient.plugin.terminplanung.internal;

import de.cherriz.master.businessclient.dialog.basic.ValideEventType;

/**
 * Created by Fredi on 26.08.2014.
 */
public enum ValideTerminplanungEventType implements ValideEventType {

    ABBRECHEN("Abbrechen"),

    FAKE_KONTAKT("Fake Kontakt"),

    INFOMATERIAL("Infomaterial"),

    TERMIN_SPEICHERN("Termin Speichern"),

    KEIN_TERMIN("Kein Termin");

    private String name;

    ValideTerminplanungEventType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}