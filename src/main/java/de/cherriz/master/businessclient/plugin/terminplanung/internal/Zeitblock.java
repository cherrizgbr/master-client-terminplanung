package de.cherriz.master.businessclient.plugin.terminplanung.internal;

/**
 * Zeitfenster im Kalender. Mit Beginn und Dauer.
 *
 * @author Frederik Kirsch
 */
public class Zeitblock {

    private int stunde;

    private int minute;

    private int dauer;

    /**
     * Konstruktor.
     * Standarddauer eines Termins sind zwei Stunden.
     *
     * @param stunde Die Stunde zu der das Zeitfenster beginnt.
     * @param minute Minute zu der das Zeitfenster beginnt.
     */
    public Zeitblock(int stunde, int minute) {
        this.stunde = stunde;
        this.minute = minute;
        this.dauer = 2;
    }

    /**
     * @return Stunde zu der das Zeitfenster beginnt.
     */
    public int getStunde() {
        return stunde;
    }

    /**
     * @return Minute zu der das Zeitfenster beginnt.
     */
    public int getMinute() {
        return minute;
    }

    /**
     * @return Dauer des Zeitfensters.
     */
    public int getDauer() {
        return dauer;
    }

    @Override
    public String toString() {
        String st = ("0" + stunde);
        st = st.substring(st.length() - 2);
        String min = ("0" + minute);
        min = min.substring(min.length() - 2);
        String to = ("0" + (stunde + dauer));
        to = to.substring(to.length() - 2);
        return st + ":" + min + " - " + to + ":" + min + " Uhr";
    }

}