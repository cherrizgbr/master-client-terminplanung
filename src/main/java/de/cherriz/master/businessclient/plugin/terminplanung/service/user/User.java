package de.cherriz.master.businessclient.plugin.terminplanung.service.user;

/**
 * Created by Fredi on 27.08.2014.
 */

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * User.
 *
 * @author Frederik Kirsch
 */
@JsonIgnoreProperties({"id", "type", "vertreter"})
public class User {

    private Integer plz = null;

    private String vorname = null;

    private String ort = null;

    private String email = null;

    private String nachname = null;

    private String telefon = null;

    private String strasse = null;

    /**
     * Leerer Konstruktor fuer generische Objekterzeugung.
     */
    public User(){
    }

    /**
     * Vollständiger Konstruktor.
     * @param vorname Der Vorname.
     * @param nachname Der Nachname.
     * @param email Die Email.
     * @param telefon Die Telefonnummer.
     * @param strasse Die Strasse.
     * @param ort Der Ort.
     * @param plz Die PLZ.
     */
    public User(String vorname, String nachname, String email, String telefon, String strasse, String ort, Integer plz) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = vorname;
        this.telefon = telefon;
        this.strasse = strasse;
        this.ort = ort;
        this.plz = plz;
    }

    /**
     * @return Der Vorname.
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * @return Der Ort.
     */
    public String getOrt() {
        return ort;
    }

    /**
     * @return Die Email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return Der Nachname.
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * @return Die Telefonnummer.
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * @return Die Strasse.
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * @return Die PLZ.
     */
    public Integer getPlz() {
        return plz;
    }

}
