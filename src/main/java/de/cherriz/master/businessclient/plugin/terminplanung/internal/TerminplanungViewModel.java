package de.cherriz.master.businessclient.plugin.terminplanung.internal;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.plugin.terminplanung.service.termin.Termin;
import de.cherriz.master.businessclient.plugin.terminplanung.service.user.User;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Das ViewModel für die UI des Plugins Terminplanung.
 * Übersetzt zwischen Modelldaten und UIProperties.
 *
 * @author Frederik Kirsch
 */
public class TerminplanungViewModel implements ViewModel {

    private StringProperty vorname = new SimpleStringProperty();

    private StringProperty nachname = new SimpleStringProperty();

    private StringProperty mail = new SimpleStringProperty();

    private StringProperty telefon = new SimpleStringProperty();

    private StringProperty strasse = new SimpleStringProperty();

    private StringProperty plz = new SimpleStringProperty();

    private StringProperty ort = new SimpleStringProperty();

    private ObjectProperty<LocalDate> date = new SimpleObjectProperty<>();

    private ObjectProperty<Zeitblock> time = new SimpleObjectProperty<>();

    private List<ValideEventListener<ValideTerminplanungEventType>> listener = null;

    private ObjectProperty<ObservableList<Zeitblock>> zeiten = new SimpleObjectProperty<>();

    private ObjectProperty<ObservableList<Termin>> termine = new SimpleObjectProperty<>();

    /**
     * Konstruktor.
     */
    public TerminplanungViewModel() {
        this.listener = new ArrayList<>();
        this.zeiten.set(FXCollections.observableArrayList(new Zeitblock(8, 0), new Zeitblock(10, 0), new Zeitblock(13, 0), new Zeitblock(15, 0), new Zeitblock(17, 0)));
        this.termine.set(FXCollections.observableArrayList(new ArrayList<>()));
    }

    /**
     * Setzt den Kontakt der in der UI dargestellt werden soll.
     * Der Kontakt ist an die Properties gebunden.
     *
     * @param kontakt Der Kontakt.
     */
    public void setKontakt(User kontakt) {
        this.vorname.set(kontakt.getVorname());
        this.nachname.set(kontakt.getNachname());
        this.mail.set(kontakt.getEmail());
        this.telefon.set(kontakt.getTelefon());
        this.strasse.set(kontakt.getStrasse());
        this.plz.set(kontakt.getPlz().toString());
        this.ort.set(kontakt.getOrt());
    }

    /**
     * Setzt die Termine die in der UI dargestellt werden sollen.
     * Die Termine sind an die Properties gebunden.
     *
     * @param termine Die Termine.
     */
    public void setTermine(List<Termin> termine) {
        this.termine.get().clear();
        this.termine.get().addAll(termine);
    }

    /**
     * Liefert den vom Benutzer gewaehlten Termin.
     *
     * @return Der Termin.
     */
    public Termin getTermin() {
        //Datum
        Instant instant = Instant.from(this.date.get().atStartOfDay(ZoneId.systemDefault()));
        Date date = Date.from(instant);
        Zeitblock zeitblock = this.time.get();

        //Zeit
        Date beginn = Calendar.getInstance().getTime();
        Date ende = Calendar.getInstance().getTime();
        beginn.setTime(date.getTime() + (zeitblock.getStunde() * 1000 * 60 * 60));
        ende.setTime(date.getTime() + (zeitblock.getStunde() * 1000 * 60 * 60) + (zeitblock.getDauer() * 1000 * 60 * 60));

        return new Termin("Termin mit " + this.vorname.get() + " " + this.nachname.get(), beginn, ende);
    }

    /**
     * Informiert darüber das die Aktion Abbrechen ausgeloest wurde.
     */
    public void cancel() {
        this.listener.forEach(listener -> listener.valideEvent(ValideTerminplanungEventType.ABBRECHEN));
    }

    /**
     * Informiert darüber das die Aktion Fake Kontakt ausgeloest wurde.
     */
    public void fake() {
        this.listener.forEach(listener -> listener.valideEvent(ValideTerminplanungEventType.FAKE_KONTAKT));
    }

    /**
     * Informiert darüber das die Aktion Termin speichern ausgeloest wurde.
     */
    public void termin() {
        this.listener.forEach(listener -> listener.valideEvent(ValideTerminplanungEventType.TERMIN_SPEICHERN));
    }

    /**
     * Informiert darüber das die Aktion Kein Termin ausgeloest wurde.
     */
    public void keinTermin() {
        this.listener.forEach(listener -> listener.valideEvent(ValideTerminplanungEventType.KEIN_TERMIN));
    }

    /**
     * Informiert darüber das die Aktion Informationsmaterial ausgeloest wurde.
     */
    public void info() {
        this.listener.forEach(listener -> listener.valideEvent(ValideTerminplanungEventType.INFOMATERIAL));
    }

    /**
     * Property des Vornamens die an den Kontakt gebunden ist.
     *
     * @return Die VornameProperty.
     */
    public StringProperty vornameProperty() {
        return this.vorname;
    }

    /**
     * Property des Nachnamens die an den Kontakt gebunden ist.
     *
     * @return Die NachnameProperty.
     */
    public StringProperty nachnameProperty() {
        return this.nachname;
    }

    /**
     * Property des Ortes die an den Kontakt gebunden ist.
     *
     * @return Die OrtProperty.
     */
    public StringProperty ortProperty() {
        return this.ort;
    }

    /**
     * Property der PLZ die an den Kontakt gebunden ist.
     *
     * @return Die PLZProperty.
     */
    public StringProperty plzProperty() {
        return this.plz;
    }

    /**
     * Property der Strasse die an den Kontakt gebunden ist.
     *
     * @return Die StrasseProperty.
     */
    public StringProperty strasseProperty() {
        return this.strasse;
    }

    /**
     * Property der Mail die an den Kontakt gebunden ist.
     *
     * @return Die MailProperty.
     */
    public StringProperty mailProperty() {
        return this.mail;
    }

    /**
     * Property der Telefonnummer die an den Kontakt gebunden ist.
     *
     * @return Die TelefonProperty.
     */
    public StringProperty telefonProperty() {
        return this.telefon;
    }

    /**
     * Property des gewaehlten Datums. Wird an den Termin gebunden.
     *
     * @return Die DatumProperty.
     */
    public ObjectProperty<LocalDate> dateProperty() {
        return this.date;
    }

    /**
     * Property des gewählten Zeitfensters. Wird an den Termin gebunden.
     *
     * @return Die Die ZeitProperty.
     */
    public ObjectProperty<Zeitblock> timeProperty() {
        return this.time;
    }

    /**
     * Property des der Zeitfenster.
     *
     * @return Die ZeitfensterProperty.
     */
    public ObjectProperty<ObservableList<Zeitblock>> zeitenProperty() {
        return this.zeiten;
    }

    /**
     * Property des Termine des Vertreters die an die Termine gebunden ist.
     *
     * @return Die TermineProperty.
     */
    public ObjectProperty<ObservableList<Termin>> termineProperty() {
        return this.termine;
    }

    /**
     * Registriert den übergebenen Listener.
     * Dieser wird über Events der UI informiert.
     *
     * @param listener The eventlistener.
     */
    public void addValideEventListener(ValideEventListener<ValideTerminplanungEventType> listener) {
        this.listener.add(listener);
    }

}