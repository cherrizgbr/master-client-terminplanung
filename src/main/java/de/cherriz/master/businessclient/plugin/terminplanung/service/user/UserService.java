package de.cherriz.master.businessclient.plugin.terminplanung.service.user;

/**
 * Service zum Zugriff auf den REST Service für User.
 *
 * @author Frederik Kirsch
 */
public interface UserService {

    /**
     * Liefert zu einer UserID das zugehoerige Objekt.
     *
     * @param id Die UserID.
     * @return Das UserObjekt.
     */
    User getUser(Long id);

}