package de.cherriz.master.businessclient.plugin.terminplanung.service.termin;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Termin zwischen Kontakt und Vertreter.
 *
 * @author Frederik Kirsch
 */
public class Termin {

    private Long id;

    private Long vertreterID;

    private Long kontaktID;

    private String beschreibung;

    private Date beginn;

    private Date ende;

    /**
     * Leerer Konstruktor für generische Objekterzeugung.
     */
    public Termin() {
    }

    /**
     * Kompletter Konstruktor.
     *
     * @param beschreibung Die Beschreibung des Termins.
     * @param beginn Der Beginn des Termins.
     * @param ende Das Ende des Termins.
     */
    public Termin(String beschreibung, Date beginn, Date ende) {
        this.beschreibung = beschreibung;
        this.beginn = beginn;
        this.ende = ende;
    }

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id Die ID des Termins.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Die ID des Vertreters.
     */
    public Long getVertreterID() {
        return vertreterID;
    }

    /**
     * Setzt die ID des Vertreters.
     * @param vertreterID Die ID des Vertreters.
     */
    public void setVertreterID(Long vertreterID) {
        this.vertreterID = vertreterID;
    }

    /**
     * @return Der Kontakt.
     */
    public Long getKontaktID() {
        return kontaktID;
    }

    /**
     * Setzt die ID des Kontakts.
     * @param kontaktID Die ID des Kontakts.
     */
    public void setKontaktID(Long kontaktID) {
        this.kontaktID = kontaktID;
    }

    /**
     * @return Die Beschreibung des Termins.
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Setzt die Beschreibung des Termins.
     * @param beschreibung Die Beschreibung des Termins.
     */
    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }


    /**
     * @return Der Terminbeginn.
     */
    public Date getBeginn() {
        return beginn;
    }

    /**
     * Setzt den Terminbeginn-
     * @param beginn Der Beginn des Termins.
     */
    public void setBeginn(Date beginn) {
        this.beginn = beginn;
    }

    /**
     * @return Das Terminende.
     */
    public Date getEnde() {
        return ende;
    }

    /**
     * Setzt das Terminende.
     * @param ende Das Terminende.
     */
    public void setEnde(Date ende) {
        this.ende = ende;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdfDatum = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat sdfZeit = new SimpleDateFormat("HH:mm");

        String result = "Beratungstermin:\n";
        result += "\tDatum: " + sdfDatum.format(this.beginn) + "\n";
        result += "\tBeginn: " + sdfZeit.format(this.beginn) + "\tEnde: " + sdfZeit.format(this.ende) + "\n";
        result += "\tBeschreibung: " + this.beschreibung + "\n";
        return result;
    }
}
