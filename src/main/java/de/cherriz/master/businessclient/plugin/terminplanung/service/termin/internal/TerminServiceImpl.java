package de.cherriz.master.businessclient.plugin.terminplanung.service.termin.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.businessclient.plugin.terminplanung.service.termin.Termin;
import de.cherriz.master.businessclient.plugin.terminplanung.service.termin.TerminService;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementierung des {@link de.cherriz.master.businessclient.plugin.terminplanung.service.termin.TerminService}.
 *
 * @author Frederik Kirsch
 */
public class TerminServiceImpl extends AbstractService implements TerminService {

    private String vertreterPath = null;

    private String createPath = null;

    /**
     * Setzt den Pfad zur Funktion Vertreter zu ID suchen.
     *
     * @param vertreterPath Der Pfad.
     */
    public void setVertreterPath(String vertreterPath) {
        this.vertreterPath = vertreterPath;
    }

    /**
     * Setzt den Pfad zur Funktion Termin speichern.
     *
     * @param createPath Der Pfad.
     */
    public void setCreatePath(String createPath) {
        this.createPath = createPath;
    }

    @Override
    public List<Termin> getTermineByVertreter(Long id) {
        List<Termin> result = new ArrayList<>();

        try {
            //Request absetzen
            String responseName = this.getTarget(this.vertreterPath.replace("{id}", id.toString()))
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);

            //Rueckgabe verarbeiten
            ObjectMapper mapper = new ObjectMapper();
            result.addAll(mapper.readValue(responseName, new TypeReference<List<Termin>>() {
            }));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Termin create(Termin termin) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String para = mapper.writeValueAsString(termin);

            //Request absetzen
            String result = this.getTarget(this.createPath)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .put(Entity.json(para), String.class);

            return mapper.readValue(result, Termin.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}