package de.cherriz.master.businessclient.plugin.terminplanung;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.shared.Plugin;
import de.cherriz.master.businessclient.plugin.terminplanung.internal.TerminplanungViewModel;
import de.cherriz.master.businessclient.plugin.terminplanung.internal.ValideTerminplanungEventType;
import de.cherriz.master.businessclient.plugin.terminplanung.service.termin.Termin;
import de.cherriz.master.businessclient.plugin.terminplanung.service.termin.TerminService;
import de.cherriz.master.businessclient.plugin.terminplanung.service.user.User;
import de.cherriz.master.businessclient.plugin.terminplanung.service.user.UserService;

import java.util.List;

/**
 * Created by Fredi on 26.08.2014.
 */
public class TerminplanungPlugin extends Plugin implements ValideEventListener<ValideTerminplanungEventType> {

    private String processKey = null;

    private String taskKey = null;

    private UserService userService = null;

    private TerminService terminService = null;

    /**
     * Loads the fxml, instantiates the view-class and binds the viewmodel.
     *
     * @param name
     * @param fxml The path to the fxml-file.
     */
    public TerminplanungPlugin(String fxml, String name, String processKey, String taskKey, UserService userService, TerminService terminService) {
        super(name, fxml, new String[]{"kundenkontaktid", "vertreterid"});
        this.processKey = processKey;
        this.taskKey = taskKey;
        this.userService = userService;
        this.terminService = terminService;
    }

    @Override
    public String getProcessDefinitionKey() {
        return this.processKey;
    }

    @Override
    public String getTaskDefinitionKey() {
        return this.taskKey;
    }

    @Override
    protected void startEditing() {
        //Für die Bearbeitung notwendigen Daten ermitteln
        Long kundenID = Long.valueOf(this.getContextAttribute("kundenkontaktid"));
        User user = this.userService.getUser(kundenID);
        Long vertreterID = Long.valueOf(this.getContextAttribute("vertreterid"));
        List<Termin> termine = this.terminService.getTermineByVertreter(vertreterID);

        //Daten zur Bearbeitung setzen
        TerminplanungViewModel viewModel = this.getViewModel();
        viewModel.setKontakt(user);
        viewModel.setTermine(termine);
    }

    @Override
    protected ViewModel initViewModel() {
        TerminplanungViewModel viewModel = new TerminplanungViewModel();
        viewModel.addValideEventListener(this);
        return viewModel;
    }

    @Override
    public void valideEvent(ValideTerminplanungEventType type) {
        switch (type) {
            case ABBRECHEN:
                this.cancelEditing();
                break;
            case FAKE_KONTAKT:
                this.finishEdition("Fake");
                break;
            case INFOMATERIAL:
                this.finishEdition("Info");
                break;
            case TERMIN_SPEICHERN:
                Long terminID = this.createTermin();
                this.finishEdition("Termin:"+terminID);
                break;
            case KEIN_TERMIN:
                this.finishEdition("Kein Termin");
                break;
        }
    }

    private Long createTermin(){
        TerminplanungViewModel viewModel = this.getViewModel();
        Termin termin = viewModel.getTermin();
        termin.setKontaktID(Long.valueOf(this.getContextAttribute("kundenkontaktid")));
        termin.setVertreterID(Long.valueOf(this.getContextAttribute("vertreterid")));

        Termin created = this.terminService.create(termin);
        return created.getId();
    }

    @Override
    public String toString() {
        return this.getName();
    }

}