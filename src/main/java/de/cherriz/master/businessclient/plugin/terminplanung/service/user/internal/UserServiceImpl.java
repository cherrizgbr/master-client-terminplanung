package de.cherriz.master.businessclient.plugin.terminplanung.service.user.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.businessclient.plugin.terminplanung.service.user.User;
import de.cherriz.master.businessclient.plugin.terminplanung.service.user.UserService;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Implementierung des {@link de.cherriz.master.businessclient.plugin.terminplanung.service.user.UserService}.
 * @author Frederik Kirsch
 */
public class UserServiceImpl extends AbstractService implements UserService {

    @Override
    public User getUser(Long id) {
        try {
            //Request absetzen
            String response = this.getTarget(id.toString())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);

            ObjectMapper mapper = new ObjectMapper();

            return mapper.readValue(response, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}